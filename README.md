# meme-api

Meme generator api.
Imagemagick is a dependency make sure it is installed.
```sh
git clone https://codeberg.org/arsenikov/meme-api && cd meme-api && mkdir out && pip install -r requirements.txt
```
To start server run,
```sh
uvicorn main:app
```