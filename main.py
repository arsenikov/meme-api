from fastapi import FastAPI
from fastapi.responses import FileResponse, HTMLResponse
from memake import make_meme
from typing import Optional
app = FastAPI()
@app.get("/")
async def read_root():
    return HTMLResponse(content="<body>it works!</body>", status_code=200)
@app.get("/memes/{meme}")
async def main(meme: str,top: Optional[str] = "",bottom: Optional[str] = "",font: Optional[str] = "impact"):
    memee = make_meme(meme,top,bottom,font)
    return FileResponse(memee)
