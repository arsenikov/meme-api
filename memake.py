import subprocess, os
def make_meme(meme,top,bottom,font):
    src = f'templates/{meme}.jpg'
    dest= f'out/{meme}.jpg'
    header= top
    footer= bottom
    afont = f'fonts/{font}.ttf'
    width= subprocess.getoutput(f'identify -format %w {src}')
    caption_height= int(width)/8
    strokewidth= int(width)/500

    os.system(f"""
        convert {src}       \
      -background none   \
      -font {afont}      \
      -fill white        \
      -stroke black      \
      -strokewidth {strokewidth}       \
      -size {width}x{caption_height}  \
        -gravity north caption:"{header}" -composite \
        -gravity south caption:"{footer}" -composite \
        {dest}
        """)
    return dest
